//
//  DatabaseManager.swift
//  Game of thrones App
//
//  Created by VYSHNAV ARAVIND on 1/18/20.
//  Copyright © 2020 VYSHNAV ARAVIND. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class DatabaseManager: NSObject {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    typealias CompletionHandler = (_ success:Bool) -> Void

     func saveInCoreDataWith(array: [[String: Any]],completionHandler: CompletionHandler) {
            for dict in array {
                _ = self.saveProgramList(dictionary: dict)
            }
        completionHandler(true)
        }
        private func saveProgramList(dictionary: [String: Any]) -> NSManagedObject? {
            let context = appDelegate.persistentContainer.viewContext
            if let program = NSEntityDescription.insertNewObject(forEntityName: "ProgramList", into: context) as? ProgramList {
                program.url = dictionary["url"] as? String ?? ""
                program.name = dictionary["name"] as? String ?? ""
                program.id = dictionary["id"] as? Int64 ?? 0
                program.season = dictionary["season"] as? Int64 ?? 0
                program.number = dictionary["number"] as? Int64 ?? 0
                program.airdate = dictionary["airdate"] as? String ?? ""
                program.airtime = dictionary["airtime"] as? String ?? ""
                program.airstamp = dictionary["airstamp"] as? String ?? ""
                program.runtime = dictionary["runtime"] as? Int64 ?? 0
                program.image = dictionary["image"] as? NSObject
                program.summary = dictionary["summary"] as? String ?? ""
                program.links = dictionary["links"] as? NSObject
                return program
            }
            return nil
        }
    
     func getSeasonPrograms(season:Int64) ->[ProgramList]?{
           let contecxt = appDelegate.persistentContainer.viewContext
           let fetchRequest:NSFetchRequest<ProgramList> = ProgramList.fetchRequest()
           var prgmList:[ProgramList] = []
           let predicate = NSPredicate(format: "season == \(season)")
           fetchRequest.predicate = predicate
        let sort = NSSortDescriptor(key: #keyPath(ProgramList.airdate), ascending: true)
        fetchRequest.sortDescriptors = [sort]
           do{
               prgmList =  try contecxt.fetch(fetchRequest)
               let ClubInfoBO = prgmList
               print(ClubInfoBO)
               return (ClubInfoBO) as? [ProgramList]
           }catch
           {
               return nil

           }
       }
    func removeAlldata(){
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ProgramList")
            fetchRequest.returnsObjectsAsFaults = false
            do {
                let results = try appDelegate.persistentContainer.viewContext.fetch(fetchRequest)
                for object in results {
                    guard let objectData = object as? NSManagedObject else {continue}
                    appDelegate.persistentContainer.viewContext.delete(objectData)
                }
            } catch let error {
                print("Detele all data in ProgramList error :", error)
            }
        }
}


    
    

    
    
