//
//  GTProgramListViewController.swift
//  Game of thrones App
//
//  Created by VYSHNAV ARAVIND on 1/18/20.
//  Copyright © 2020 VYSHNAV ARAVIND. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class GTProgramListViewController: UIViewController,UIScrollViewDelegate
 {

    @IBOutlet var headerCollectionView:UICollectionView!
    @IBOutlet var programListTableview:UITableView!
    @IBOutlet var seasonLbl:UILabel!

    
    let database = DatabaseManager()
    var scrollView = UIScrollView()
    var yPosition:CGFloat = 0
    var scrollViewContentSize:CGFloat=0;
    
    var programList : [ProgramList]?
    var selectedSeason = 0
    var popUpView: UIView?
    var lastSelectedIndex = IndexPath(row: 0, section: 0)
    var selectedIndex = IndexPath()

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customUI()
        selectedIndex = IndexPath(row: selectedSeason, section: 0)
        headerCollectionView.reloadData()
        if lastSelectedIndex != selectedIndex{
            headerCollectionView.reloadItems(at: [lastSelectedIndex,selectedIndex])
        }
        headerCollectionView.scrollToItem(at: IndexPath(row:selectedSeason, section:0), at: .centeredHorizontally, animated: true)
        loadDataFromCoredata()
        navigationController?.navigationBar.barTintColor = UIColor.black
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]

      //  self.getProgramListAPI()
    }
    

    // MARK: - Functions
    
    func customUI(){
        headerCollectionView.dataSource = self
        headerCollectionView.delegate = self
        programListTableview.dataSource = self
        programListTableview.delegate = self
    }
    func loadDataFromCoredata(){
        seasonLbl.text = "Season : "+(String(selectedSeason+1))
        
        self.programList = self.database.getSeasonPrograms(season: Int64(selectedSeason+1))
        reloadData()
    }
    func reloadData(){
        programListTableview.setContentOffset(.zero, animated: true)
        programListTableview.reloadData()
        
    }
    
    func generatePopUp() {
        if popUpView == nil {
            popUpView  = UIView()
            popUpView!.backgroundColor = .white
            popUpView!.layer.cornerRadius = 8
            popUpView!.layer.masksToBounds = true
            self.view.addSubview(popUpView!)
            
            popUpView!.translatesAutoresizingMaskIntoConstraints = false
            popUpView!.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
            popUpView!.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            popUpView!.widthAnchor.constraint(equalToConstant: view.bounds.width - 30).isActive = true
            popUpView!.heightAnchor.constraint(equalToConstant: view.bounds.height * (3/4)).isActive = true
            
            let dismissButton = UIButton(type: .custom)
            dismissButton.setTitle("Dismiss", for: .normal)
            dismissButton.setTitleColor(.black, for: .normal)
            dismissButton.addTarget(self, action: #selector(dismissPressed), for: .touchUpInside)
            popUpView!.addSubview(dismissButton)
            
            dismissButton.translatesAutoresizingMaskIntoConstraints = false
            dismissButton.bottomAnchor.constraint(equalTo: popUpView!.bottomAnchor, constant: 16).isActive = true
            dismissButton.centerXAnchor.constraint(equalTo: popUpView!.centerXAnchor).isActive = true
            dismissButton.widthAnchor.constraint(equalToConstant: 150).isActive = true
            dismissButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        }
        
        if popUpView?.transform != .identity {
            popUpView!.transform = CGAffineTransform(translationX: 0, y: self.view.bounds.height)
            
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
                self.popUpView!.transform = .identity
            }) { (true) in
                
            }
        }
    }
    
    @objc func dismissPressed() {
        if popUpView?.transform == .identity {
            
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
                self.popUpView!.transform = CGAffineTransform(translationX: 0, y: self.view.bounds.height)
            }) { (true) in
                
            }
        }
    }
    
    
}
// MARK: - UICollectionview Datasource and Delegates
extension GTProgramListViewController:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 8
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GTSeasonHeaderCollectionViewCell", for: indexPath)as! GTSeasonHeaderCollectionViewCell
        cell.seasonNameLbl.text = String(indexPath.row+1)
        if selectedIndex == indexPath{
            cell.bgView.backgroundColor = UIColor.init(hex: "865A2E")
        }else{
            cell.bgView.backgroundColor = UIColor.init(hex: "808080")
        }
        return cell
    }
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: 80, height: 80)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        selectedSeason = indexPath.row
        
        lastSelectedIndex = selectedIndex
        selectedIndex = indexPath
        if lastSelectedIndex != selectedIndex{
                   headerCollectionView.reloadItems(at: [lastSelectedIndex,selectedIndex])
               }
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        loadDataFromCoredata()

       }
    
}

// MARK: - UITableview Datasource and Delegates
extension GTProgramListViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.programList?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GTProgramListTableViewCell", for: indexPath)as! GTProgramListTableViewCell
        let item = programList?[indexPath.row]
        if item != nil{
            cell.nameLbl.text = item?.name ?? ""
            cell.arrivedDateLbl.text = "Aired Date : "+(item?.airdate ?? "")
            
           let image = item?.image as? [String:Any] ?? [:]
            loadImage(stringUrl: image["medium"]as? String ?? "", imageview: cell.programImageView)
           // cell.descLbl.attributedText = (item?.summary ?? "").convertHtmlToAttributedStringWithCSS(font: UIFont.systemFont(ofSize: 17, weight: .medium), csscolor: "white", lineheight: 5, csstextalign: "center")

            let font = UIFont(name: "Poppins-Medium", size: 16)!
                   do {
                       let attr = try NSMutableAttributedString(HTMLString: item?.summary ?? "", font: font)
                       var attrs = attr?.attributes(at: 0, effectiveRange: nil)
                       attrs?[NSAttributedString.Key.font]
                       cell.descLbl.attributedText = attr
                   }catch{
                       print("error")
                   }
            cell.descLbl.textColor = .white
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.generatePopUp()
    }
    
}
