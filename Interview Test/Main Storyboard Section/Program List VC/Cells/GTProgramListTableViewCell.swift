//
//  GTProgramListTableViewCell.swift
//  Game of thrones App
//
//  Created by VYSHNAV ARAVIND on 1/18/20.
//  Copyright © 2020 VYSHNAV ARAVIND. All rights reserved.
//

import UIKit

class GTProgramListTableViewCell: UITableViewCell {
    @IBOutlet var nameLbl:UILabel!
    @IBOutlet var arrivedDateLbl:UILabel!
    @IBOutlet var programImageView:UIImageView!
    @IBOutlet var descLbl:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
