//
//  GTSeasonHeaderCollectionViewCell.swift
//  Game of thrones App
//
//  Created by VYSHNAV ARAVIND on 1/18/20.
//  Copyright © 2020 VYSHNAV ARAVIND. All rights reserved.
//

import UIKit

class GTSeasonHeaderCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var bgView:UIView!
    @IBOutlet var seasonNameLbl:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        bgView.makeCirularView()
    }
}
