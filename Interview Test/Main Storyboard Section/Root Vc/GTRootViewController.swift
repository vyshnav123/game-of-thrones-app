//
//  GTRootViewController.swift
//  Game of thrones App
//
//  Created by VYSHNAV ARAVIND on 1/18/20.
//  Copyright © 2020 VYSHNAV ARAVIND. All rights reserved.
//

import UIKit
import Alamofire
import Foundation

class GTRootViewController: UIViewController {
    let programUrl = "https://api.tvmaze.com/shows/82/episodes"
    let database = DatabaseManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getProgramListAPI()
    }
    func setNavBar(){
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().backgroundColor = .black
        let font:UIFont = UIFont(name: "Poppins-medium", size: 18.0)!
        let navbarTitleAtt = [
            NSAttributedString.Key.font:font,
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]
        UINavigationBar.appearance().titleTextAttributes = navbarTitleAtt
        
    }
    func openPageViewController(){
        if #available(iOS 13.0, *) {
            let pageVc = storyboard?.instantiateViewController(identifier: "GTPageViewController")as! GTPageViewController
            let navVC = UINavigationController.init(rootViewController: pageVc)
            UIApplication.shared.windows.first?.rootViewController = navVC
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        }else{
            let pageVc = storyboard?.instantiateViewController(withIdentifier: "GTPageViewController")as! GTPageViewController
            let navVC = UINavigationController.init(rootViewController: pageVc)
            UIApplication.shared.windows.first?.rootViewController = navVC
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        }      }
    // MARK: - API Calls
    func getProgramListAPI(){
        if isInternetAvailable(){
            creteLoader(vc: self)
            showLoading(vc: self)
            var headers: HTTPHeaders = [
                "Content-Type": "application/x-www-form-urlencoded"
            ]
            
            Alamofire.request(URL(string: programUrl)!,method: .get,parameters: nil,headers:headers)
                .responseJSON { (response) -> Void in
                    if response.result.isSuccess {
                        print(response.result.value)
                        let result = response.result.value as? [[String:Any]] ?? [[:]]
                        if result.count > 0{
                            self.database.removeAlldata()
                            self.database.saveInCoreDataWith(array: result) { (status) in
                                print("success")
                                hideLoading(vc: self)
                                self.openPageViewController()
                            }
                        }
                    }
                    else{
                        //error(response.error)
                    }
            }
            
        }else{
            noInternetAlert(self) { (status) in
                if status ?? false{
                    self.getProgramListAPI()
                }
            }
            
        }
    }
    
    
}
