//
//  GTPageController.swift
//  Game of thrones App
//
//  Created by VYSHNAV ARAVIND on 1/18/20.
//  Copyright © 2020 VYSHNAV ARAVIND. All rights reserved.
//

import Foundation
import UIKit

class GTPageViewController: UIViewController, UIPageViewControllerDataSource{
    var pageViewController : UIPageViewController?
   // var sectionArray : [ProgramList]?
    var pageIndex: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageViewController!.dataSource = self
        let startingViewController: GTProgramListViewController = getViewControllerAtIndex(index: pageIndex)
        let viewControllers = [startingViewController]
        pageViewController!.setViewControllers(viewControllers , direction: .forward, animated: false, completion: nil)
        
        pageViewController!.view.frame = self.view.frame
        addChild(pageViewController!)
        view.addSubview(pageViewController!.view)
        pageViewController!.didMove(toParent: self)
        self.title = "Game of Thrones"
        self.view.backgroundColor = UIColor.clear
        
        navigationController?.navigationBar.barTintColor = UIColor.black
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
              // Do any additional setup after loading the view.
    }
   
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let pageContent: GTProgramListViewController = viewController as! GTProgramListViewController
        var index = pageContent.selectedSeason
        if (index == NSNotFound)
        {
            return nil;
        }
        index += 1;
        if (index == 8)
        {
            return nil;
        }
        return getViewControllerAtIndex(index: index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let pageContent: GTProgramListViewController = viewController as! GTProgramListViewController
        var index = pageContent.selectedSeason
        
        if ((index == 0) || (index == NSNotFound))
        {
            return nil
        }
        index -= 1;
        return getViewControllerAtIndex(index: index)
    }
    
       func getViewControllerAtIndex(index: NSInteger) -> GTProgramListViewController
    {
        let pageContentViewController =  self.storyboard?.instantiateViewController(withIdentifier: "GTProgramListViewController") as! GTProgramListViewController
        pageContentViewController.selectedSeason = index
        return pageContentViewController
    }
}
