//
//  ProgramList+CoreDataProperties.swift
//  
//
//  Created by VYSHNAV ARAVIND on 1/18/20.
//
//

import Foundation
import CoreData


extension ProgramList {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ProgramList> {
        return NSFetchRequest<ProgramList>(entityName: "ProgramList")
    }

    @NSManaged public var url: Int64
    @NSManaged public var name: String?
    @NSManaged public var id: Int64
    @NSManaged public var season: Int64
    @NSManaged public var number: Int64
    @NSManaged public var airdate: String?
    @NSManaged public var airtime: String?
    @NSManaged public var airstamp: String?
    @NSManaged public var runtime: Int64
    @NSManaged public var image: NSObject?
    @NSManaged public var summary: String?
    @NSManaged public var links: NSObject?

}
