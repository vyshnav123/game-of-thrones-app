//
//  GTCommon.swift
//  Game of thrones App
//
//  Created by VYSHNAV ARAVIND on 1/18/20.
//  Copyright © 2020 VYSHNAV ARAVIND. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
import SystemConfiguration

var indicator = UIActivityIndicatorView(style: .gray)


//MARK:- UIView
extension UIView {
    /**
     Make the image view circular
     */
    func makeCirularView() {
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.borderWidth = 1
        self.layer.masksToBounds = true
        self.layer.cornerRadius = self.frame.size.width/2
    }
}
extension UIColor {
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        var rgbValue: UInt64 = 0
        scanner.scanHexInt64(&rgbValue)
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
    
    func kBgColor() -> UIColor{
        return UIColor(red: 106/255, green: 207/255, blue: 90/255, alpha: 1.0)
    }
}

    extension NSAttributedString {
        public convenience init?(HTMLString html: String, font: UIFont? = nil) throws {
            let options : [NSAttributedString.DocumentReadingOptionKey : Any] =
                [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html,
                 NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue]
            
            guard let data = html.data(using: .utf8, allowLossyConversion: true) else {
                throw NSError(domain: "Parse Error", code: 0, userInfo: nil)
            }
            
            if let font = font {
                guard let attr = try? NSMutableAttributedString(data: data, options: options, documentAttributes: nil) else {
                    throw NSError(domain: "Parse Error", code: 0, userInfo: nil)
                }
                var attrs = attr.attributes(at: 0, effectiveRange: nil)
                attrs[NSAttributedString.Key.font] = font
                attr.setAttributes(attrs, range: NSRange(location: 0, length: attr.length))
                self.init(attributedString: attr)
            } else {
                try? self.init(data: data, options: options, documentAttributes: nil)
            }
        }
    }



//MARK:- Load Image
func loadImage(stringUrl:String,imageview:UIImageView){
    let imageUrl = stringUrl.replacingOccurrences(of: " ", with: "%20")
    imageview.sd_setShowActivityIndicatorView(true)
    imageview.sd_setIndicatorStyle(.gray)
    imageview.sd_setImage(with: URL(string: imageUrl),placeholderImage: UIImage.init(named: "placeholder_image"))
}
func creteLoader(vc:UIViewController){
    indicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0)
    indicator.center = vc.view.center
    vc.view.addSubview(indicator)
    indicator.bringSubviewToFront(vc.view)
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
}
func showLoading(vc:UIViewController){
    vc.view.isUserInteractionEnabled = false
    indicator.startAnimating()
}
func hideLoading(vc:UIViewController){
    vc.view.isUserInteractionEnabled = false
    indicator.startAnimating()
}

func isInternetAvailable() -> Bool
{
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
            SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
    }
    
    var flags = SCNetworkReachabilityFlags()
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
        return false
    }
    let isReachable = flags.contains(.reachable)
    let needsConnection = flags.contains(.connectionRequired)
    return (isReachable && !needsConnection)
}

func noInternetAlert(_ vc: UIViewController, completion: ((_ success:Bool?) -> Void)?) {
    let alertView = UIAlertController(title: "No Internet Connection", message: "We can not detect any internet connectivity.Please check your internet connection and try again.", preferredStyle: UIAlertController.Style.alert)
    alertView.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive) { UIAlertAction  in
        completion?(false)
    })
    alertView.addAction(UIAlertAction(title: "Retry", style: UIAlertAction.Style.default, handler: { (action) -> Void in
        completion?(true)
    }))
    vc.present(alertView, animated: true, completion: nil)
}
